import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from '../route.js'

Vue.use(VueRouter);

Vue.config.productionTip = false

new Vue({
  router:VueRouter,
  vuetify,
  render: h => h(App)
}).$mount('#app')
