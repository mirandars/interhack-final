import VueRouter from "vue-router"
import Vue from "vue"
import EB from "./src/components/EB.vue"
import EM from "./src/components/EM.vue"
import ES from "./src/components/ES.vue"
import Pitagoras from "./src/components/Pitagoras.vue"
import Vetores from "./src/components/Vetores.vue"
import VetoresDark from "./src/components/VetoresDark.vue"
import Francesa from "./src/components/Francesa.vue"
import Dom from "./src/components/Dom.vue"
import SujeitoPred from "./src/components/SujeitoPred.vue"
import AppDark from "./src/components/AppDark.vue"
import Main from "./src/App.vue"


Vue.use (VueRouter);

const router = new VueRouter({
    mode: "hash",
    routes: [
        {path: '/', component: Main},
      { path: '/EnsinoBasico', name:"Basico", component: EB },
      { path: '/EnsinoMedio', name:"Medio", component: EM },
      { path: '/EnsinoSuperior', name:"Superior", component: ES },
      { path: '/Pitagoras', name:"Pitagoras", component: Pitagoras},
      { path: '/Vetores', name:"Vetores", component: Vetores},
      { path: '/VetoresDark', name:"VetoresDark", component: VetoresDark},
      { path: '/Francesa', name:"Francesa", component: Francesa},
      { path: '/Dom', name:"Dom", component: Dom},
      { path: '/SujeitoPred', name:"SujeitoPred", component: SujeitoPred},
      { path: '/AppDark', name:"AppDark", component: AppDark}
    ]
  })

  export default router